var express = require('express');
var app = express();
var jsonfile = require('jsonfile')
var recursive = require('recursive-readdir');
var fs = require('fs');
var mysql = require('mysql');
var alarmTime = "18:33";
var alarmSource = "";
var toBePlayed = "";
var player = {playing: false, folder: "", source: "", pause: false, volume: 1};
var storedData = {alarmTime: "", alarmSource: ""};
var alarmPlayer = {playing: true, folder: ""};
var jsonFileStore = __dirname + "/data/data.json";
var jsonBoxColor = __dirname + "/data/boxcolor.json";
var jsonTextColor = __dirname + "/data/textcolor.json";
var jsonBackgroundImg = __dirname + "/data/background.json";


var currentArray = "";
var musicFolder = 'client/music';
var backgroundFolder = 'client/backgrounds';
var path = require('path');
var monitor = false;
var speaker = false;
var light = false;
var exec = require('child_process').exec;
url = require('url');
var sources = {}
var musicSources;
app.listen(3000);


read();
turnOnMonitor();
getMusicSources();


function write(){
    storedData.alarmTime = alarmTime;
    storedData.alarmSource = alarmSource;
    jsonfile.writeFile(jsonFileStore, storedData, function (err) {
        console.error(err)
    })
}

function turnOnMonitor() {
    monitor = true;
    console.log("zapinam monitor");
    shellComands("python python/zapnout1.py") // python zapni2.py
}
function read() {
    jsonfile.readFile(jsonFileStore, function (err, obj) {
        console.log(obj);
        alarmTime = obj.alarmTime;
        alarmSource = obj.alarmSource;
    })
}


function getMusicSources() {
    fs.readdir(musicFolder, (err, files) => {
        musicSources = files;
        files.forEach(function (file) {
            recursive(musicFolder + "/" + file, function (err, files) {
                sources[file] = files;
            });
        });

    });
}


//connection.connect();
function checkAlarm() {
    var now = getCurrentTime();
    if (now.localeCompare(alarmTime) == 0) {
        return true;
    } else {
        return false;
    }
}
function getCurrentTime() {
    var now = new Date();
    var minutes = now.getMinutes();
    var hours = now.getHours();

    if (hours < 10) {
        hours = 0 + "" + hours;
    }
    if (minutes < 10) {
        minutes = 0 + "" + minutes;
    }

    var time = hours + ":" + minutes;
    return time;
}
app.use("/server", express.static('server'));
app.use("/client", express.static('client'));
//app.use("/public/images",express.static('/public/images'));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8080');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


app.get('/devices', function (req, res) {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/html/devices.html'));
});
app.get('/alarm', function (req, res) {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/html/alarm.html'));
});
app.get('/music', function (req, res) {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/html/music.html'));
});
app.get('/settings', function (req, res) {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/html/settings.html'));
});



app.get('/getcssdata', function (req, res) {
    console.log(__dirname);
    res.writeHead(200, {"Content-Type": "application/json"});
    fs.readFile(path.join(__dirname + '/../client/style/schema.css'), 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        var attributes = data.split("}");
        res.end(JSON.stringify(attributes));
    });


});




app.get('/usablebackgrounds', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    fs.readdir(backgroundFolder, (err, files) => {
        res.end(JSON.stringify(files))
    });
});



app.get('/saveboxcolor', function (req, res) {
    var boxColor = req.query.boxcolor;
    console.log(boxColor);
    jsonfile.writeFile(jsonBoxColor, boxColor, function (err) {
     console.error(err)
        res.end("done");
     })
});

app.get('/savetextcolor', function (req, res) {
    var txtColor = req.query.textcolor;
    console.log("ukládám barvu");
    jsonfile.writeFile(jsonTextColor, txtColor, function (err) {
        console.error(err)
        res.end("done");
    })
});

app.get('/savebackground', function (req, res) {
    var background = req.query.background;
    console.log(background);
    console.log("Background: " + req.query.background);
    jsonfile.writeFile(jsonBackgroundImg, background, function (err) {
        console.error(err)
        res.end("done");
    })
});















app.get('/getboxcolor', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    jsonfile.readFile(jsonBoxColor, function (err, obj) {
        res.end(obj);
    })
});

app.get('/getBackgroundImg', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    jsonfile.readFile(jsonBackgroundImg, function (err, obj) {
        res.end(obj);
    })
});

app.get('/getTextColor', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    jsonfile.readFile(jsonTextColor, function (err, obj) {
        res.end(obj);
    })
});









app.get('/getCommands', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    if (checkAlarm()) {
        if (!monitor) {
            turnOnMonitor();
        }
        if (player.folder.localeCompare(alarmSource) != 0) {
            play(alarmSource);
        }
        player = alarmPlayer;
    }
    res.end(JSON.stringify(player));

});

app.get('/playpause', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    player["playing"] = !player["playing"];
    res.end(JSON.stringify(player));
});

app.get('/nextsong', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    var length = currentArray.length;
    var index = Math.floor((Math.random() * length));
    player["source"] = currentArray[index];
    res.end("done");
});

app.get('/play', function (req, res) {
    var name = req.query.name;
    play(name);
    res.end("end");
});
/*
app.get('/play' + '', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    player["source"] = "";
    player["playing"] = true;
    console.log(" play request");

    recursive(musicFolder, function (err, files) {
        toBePlayed = files;
        res.end(JSON.stringify(toBePlayed));
    });
});
*/
function  play(name) {
    currentArray = sources[name];
    var length = currentArray.length;
    var index = Math.floor((Math.random() * length));
    playerfolder = name;
    console.log(currentArray[index]);
    player["folder"] = name;
    player["playing"] = true;
    player["source"] = currentArray[index];
}


app.get('/getPlaySources', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    res.end(JSON.stringify(toBePlayed));

});


app.get('/playlists', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    console.log("get playlsit request");
    var name = req.param('name');
    var slozka = musicFolder + name + "/";
    fs.readdir(musicFolder + name + "/", (err, files) => {
        recursive(slozka, function (err, files) {
            res.end(JSON.stringify(files));
        });
    });
});
app.get('/getplaylists', function (req, res) {
    console.log("get playlsit request");
  //  var name = req.param('name');
  //  var slozka = musicFolder + name + "/";
    fs.readdir(musicFolder, (err, files) => {
        res.end(JSON.stringify(files));
    });
});


app.get('/index', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
app.get('/settime', function (req, res) {
    alarmTime = req.query.time;
    alarmSource = req.query.playlist;
    write();
    res.end("done");

});
app.get('/getalarmtime', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    res.end(JSON.stringify(alarmTime));
});
app.get('/statuses', function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json"});
    var array = {monitor: monitor, light: light, speaker: speaker};
    res.end(JSON.stringify(array));
});


app.get('/lights/:state', function (req, res) {
    var state = req.params.state;

    if (light) {
        console.log("vypinam svetla")
        shellComands("python python/vypnout2.py") // python zapni2.py
    } else if (!light) {
        console.log("zapinam svetla")
        shellComands("python python/zapnout2.py") // python zapni2.py
    }
    light = !light;
    res.end("done");
});


app.get('/monitor', function (req, res) {
    if (monitor) {
        console.log("vypinam monitor")
        shellComands("python python/vypnout1.py") // python zapni2.py
    } else if (!monitor) {
        console.log("zapinam monitor")
        shellComands("python python/zapnout1.py") // python zapni2.py
    }
    monitor = !monitor;
    res.end("done");
});

app.get('/speakers/:state', function (req, res) {
    var state = req.params.state;
    if (speaker) {
        console.log("vypinam reproduktory")
        shellComands("python python/vypnout3.py") // python zapni2.py
    } else if (!speaker) {
        console.log("zapinam reproduktory")
        shellComands("python python/zapnout3.py") // python zapni2.py
    }
    speaker = !speaker;
    res.end("done");
});


function shellComands(command) {
    exec(command, function (error, stdout, stderr) {
        console.log(stdout);
    });
}
