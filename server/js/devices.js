
var monitor = false;
var speaker = false;
var light = false;
var urlSource = window.location.href;
urlSource = urlSource.substr(0,urlSource.lastIndexOf(":")+5);
console.log(urlSource);

var monitorDiv = document.getElementById("monitor");
var lightDiv = document.getElementById("light");
var speakerDiv = document.getElementById("speaker");

$.getJSON("http://127.0.0.1:3000/getalarmtime", function(data) {
    console.log(data);
});

function powerButton() {
    setLights(1);
}

setInterval(downloadsColorIndicators, 1000);



function downloadsColorIndicators(){
    var url = urlSource+"/statuses";
    $.getJSON(url, function(data) {
        monitor = data["monitor"];
        light = data["light"];
        speaker = data["speaker"];
        updateColors();
    });
}

function updateColors(){
    if(light){
        lightDiv.style.backgroundColor = "green";
    }else{
        lightDiv.style.backgroundColor = "red";
    }
    if(monitor){
        monitorDiv.style.backgroundColor = "green";
    }else{
        monitorDiv.style.backgroundColor = "red";
    }
    if(speaker){
        speakerDiv.style.backgroundColor = "green";
    }else{
        speakerDiv.style.backgroundColor = "red";
    }
}


function setLight(state){
    var url = urlSource+"/lights/"+state;
    $.get(url, function(data, status){
        downloadsColorIndicators();
    });
}
function setSpeaker(state){
    var url = urlSource+"/speakers/"+state;
    $.get(url, function(data, status){
        downloadsColorIndicators();
    });
}

function setMonitor(state){
    var url = urlSource+"/monitor";
    $.get(url, function(data, status){
        downloadsColorIndicators();
    });
}
