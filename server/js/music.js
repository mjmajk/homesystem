var musicList = document.getElementById("music");
var youtubeList = document.getElementById("youtube");
var radioList = document.getElementById("radio");
var playPause = document.getElementById("playpause");
var classPlay = "fa fa-play-circle fa-5x";
var classPause = "fa fa-pause-circle fa-5x";
var playing = false;
var baseURL = "http://"+window.location.host;

var playButtonSrc = "server/images/music/play.png";
var pauseButtonSrc = "server/images/music/pause.png";

downloadMusicFolders();
getPlayingInformation();

function add( destList,  name){
    var li = document.createElement("li");
    var a = document.createElement("a");
    a.href = "#";
    a.innerHTML = name;
    a.name = name;
    a.onclick = function () {
        startPlaying(name);
    };
    li.appendChild(a);
    destList.appendChild(li);
}
function getPlayingInformation(){
  var url = baseURL+"/getCommands";
  $.getJSON(url,function(result) {
    console.log(result);
      if(result.playing){
          play();
      }
  })
}

function playPauseFunction(){
    var url = baseURL + "/playpause";
    $.get(url, function (result) {
        console.log("done");
    });
    if(playing){
        pause();
    }else{
        play()
    }
}
function pause(){
    playing = false;
    console.log("pause");
    playPause.src = "server/images/music/play.png";
}
function play(){
    playing = true;
    playPause.src = "server/images/music/pause.png";
}
function startPlaying(name){
    console.log("start play");
    play();
    var url = baseURL + "/play?name="+name;
     $.get(url, function (result) {
        console.log("done");
    });
}


function nextSong() {
    var url = baseURL + "/nextSong";
    $.getJSON(url, function (result) {
        console.log("done");
    });
}

function downloadMusicFolders() {
var url = baseURL + "/getplaylists";
console.log(url);
    $.getJSON(url, function (result) {
        $.each(result, function(i, field){
            if(field.localeCompare("desktop.ini")!=0){
                add(musicList,field);
            }

        });
    })
}
