var urlSource = "http://"+window.location.host;
var timeInput = document.getElementById("time-picker");
var playlistSelector = document.getElementById("playlistSelect");
downloadPlaylists();
downloadAlarmTime();
function downloadAlarmTime(){
    var url = urlSource+"/getalarmtime";
    console.log(url);
    $.getJSON(url, function(data) {
        console.log(data);
        timeInput.value = data;
    });

}



function add( destList,  name){
    var option = document.createElement("option");
    option.innerHTML = name;
    destList.appendChild(option);
}

function downloadPlaylists() {
    var url = urlSource + "/getplaylists";
    console.log(url);
    $.getJSON(url, function (result) {
        console.log(result);
        $.each(result, function(i, field){
                add(playlistSelector,field);
        });
    })
}


function setAlarmTime(){
    var time= encodeURIComponent(timeInput.value);
    var e = document.getElementById("playlistSelect");
    var playlist = e.options[e.selectedIndex].text;
    var url = urlSource+"/settime?time="+time+"&playlist="+playlist;
    console.log(url);
    $.getJSON(url, function(data) {
        console.log(data);

    });
}


