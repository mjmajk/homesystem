/**
 * Created by Michal on 13.03.2017.
 */

import {combineReducers} from "redux"
import time from './time';
export default combineReducers({time});
