/**
 * Created by Michal on 13.03.2017.
 */

const initialState = {
    alarmTime: "08:00"
};

export default function timeStore(state = initialState, action = {}) {
    const {type, payload} = action;
    switch (type) {
        case 'SET_ALARM_TIME': {
            return {
                ...state,
                alarmTime: payload.alarmTime
            }
        }
        default: return state;
    }
}
