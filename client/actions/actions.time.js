/**
 * Created by Michal on 13.03.2017.
 */

const baseUrl = "http://127.0.0.1:3000/";
import Request from "superagent";

export function fetchAlarmTime() {
    return (dispatch, getState) => {
        console.log(getState.time);
        const url = baseUrl + "getalarmtime";
        Request.get(url).then((response) => {
            console.log('time response');
            console.log(response);
            dispatch(setAlarmTime(response.body));
        });
    }
}

export function setAlarmTime(alarmTime) {
    return {
        type: 'SET_ALARM_TIME',
        payload: {alarmTime}
    }
}

