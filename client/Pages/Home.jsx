import React from "react";

import BackgroundImage from '../components/BackgroundImage';
import BoxColor from '../components/BoxColor';
import TextColor from '../components/TextColor';

export default class Home extends React.Component {
    constructor() {
            super();
            this.state = {
                view : false,
                color: {
                    r: '241',
                    g: '112',
                    b: '19',
                    a: '1',
                },
            }




    }
    changeState = () => {
        this.setState({view: !this.state.view});
        console.log(this.state.view);
        console.log("hi");
    }

    render() {
        const divStyle = {
            height: "1080px",
        };
        const buttonStyle = {

            position: 'absolute',
            right: "0px",
            display: 'block',
            cursor: 'pointer',
            zIndex: "4"
        };

        const settingsBackground = {
            position: 'absolute',
            right: "10px",
            top: "15px",
            zIndex: "0",
            height: "200px",
            width: "200px",
            backgroundColor: "black",
            borderRadius: "10px"
        }
        return (
            <div style={divStyle}>
                <div onClick={this.changeState} style={buttonStyle}>
                    {this.state.view ?
                    <img src="./client/images/close.png" width="30px" alt=""/>
                        :
                        <img src="./client/images/settings.png" width="30px" alt=""/> }
                </div>
                <TextColor view={this.state.view}/>
                <BoxColor view={this.state.view}/>

                <BackgroundImage view={this.state.view}/>
                { this.state.view ?
                <div style={settingsBackground}></div>
                    : null}


            </div>
        );
    }
}
