import React from "react";
export default class Music extends React.Component {


    render() {
        const divStyle = {

            height: "1080px",
            backgroundImage: 'url("https://allwallpapers.info/wp-content/uploads/2016/05/6780-microphone-1920x1080-music-wallpaper.jpeg")'
        };

        return (
            <div style={divStyle} className="music">
                <h1><a name="music">Music</a></h1>
            </div>
        );
    }
}
