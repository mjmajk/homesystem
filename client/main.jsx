import React from 'react';
import ReactDOM from 'react-dom';
import Home from './Pages/Home';
import Music from './Pages/music';
import PublicTransport from './Pages/publicTransport'
import Reminder from './Pages/reminder'
import KeyHandler, {KEYPRESS, KEYDOWN} from 'react-key-handler';


class Layout extends React.Component {

    render() {

        const divStyle = {
            overflow: "hidden"
        };

        return (
            <div style={divStyle}>
                <Home/>
            </div>
        );
    }
}

const app = document.getElementById('app');
ReactDOM.render(<Layout/>, app);


