import React from "react";
var axios = require('axios');

export default class Schema extends React.Component {
    constructor() {
        super();
        this.state = {
            color: {
                r: '241',
                g: '112',
                b: '19',
                a: '1',
            },
        }
    }


    render() {
        const divStyle = {
            fontSize: "180px",
            textAlign: "left",


        };
        return (
            <div className="player-box text-color">
                <div>
                    <img id="img-play" src="client/images/triangle.png" width="100px" alt=""/>
                </div>
                <span id="artist">{this.state.artist}</span>
                <span id="song">{this.state.song}</span>
            </div>
        );

    }
}
