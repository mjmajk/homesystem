import React from "react";
import TimeView from './TimeView';
import Temparature from '../components/Temparature';
import AlarmTime from '../components/AlarmTime';
import Player from '../components/Player';
var InputColor = require('react-input-color');
var axios = require('axios');
export default class TextColor extends React.Component {
    constructor() {
        super();
        this.state = {
            color: "#ffe6e6",
            url: "http://"+window.location.host
        }
        this.handleChange = this.handleChange.bind(this);
        this.getTextColor();
    }


    getTextColor = () =>{
        var url = this.state.url+"/getTextColor";
        axios.get(url)
            .then(res => {
                var color = "#"+res.data;
                console.log(color);
                this.setState({color: color});
            });
    }

    save = (color) => {
        console.log("ziskana barva: " + color);
        color = color.substr(1,6);
        var url = +this.state.url+"/savetextcolor?textcolor=" + color;
        console.log(url);
        axios.get(url)
            .then(function (response) {

                }
            );
    }



    handleChange = (e) =>{
        this.setState({color: e.target.value});
        console.log("handle change " + e.target.value);
        this.save(e.target.value);

    }
    render() {
        const divStyle = {
            position: "absolute",
            right: "40px",
            top: "90px",
            zIndex: "4",


        };
        const popisek = {
            position: "absolute",
            right: "60px",
            width: "80px",
            top: "4px",
            color: "white"
        };
        return (
            <div>
                { this.props.view ?
                    <div style={divStyle}>
                        <span style={popisek}>Barva textu</span>
                        <input
                            type="color"
                            value={this.state.color}
                            onChange={this.handleChange}
                        />
                    </div>

                    : null }
                <TimeView color={this.state.color}/>
                <Temparature color={this.state.color}/>
                <AlarmTime color={this.state.color}/>
                <Player color={this.state.color}/>
            </div>
        );

    }
}
