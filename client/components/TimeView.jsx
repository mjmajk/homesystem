import React from "react";
import {fetchAlarmTime} from '../actions/actions.time';

import {connect} from "react-redux";

class TimeView extends React.Component {

    state = {
        date: new Date(),
        hours: "",
        minutes: ""
    };

    formatTime(time) {
        var minutes = time.getMinutes();
        var hours = time.getHours();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (hours < 10) {
            hours = "0" + hours;
        }
        this.setState({hours: hours, minutes: minutes});
    }

    componentDidMount() {
        this.formatTime(new Date);
        setInterval(() => {
            this.formatTime(new Date);
        }, 60000);
    }
    


    render() {
        const divStyle = {
            textAlign: "center",
            color: `${ this.props.color}`,
            fontFamily: "Myriad Pro",
            position: "absolute",
            width: "100%",
            fontSize: "750px",
            top: "-50px",
        };

        const {date} = this.state;
        return (

            <div style={divStyle}>{this.state.hours + ":" + this.state.minutes}</div>

        );
    }

}

export default TimeView;