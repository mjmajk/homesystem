import React from "react";
import ReactPlayer from 'react-player';
import ReactInterval from 'react-interval';
import Request from "superagent";
import ReactAudioPlayer from 'react-audio-player';
import PlayerBox from '../components/PlayerBox';
import {connect} from "react-redux";

class Player extends React.Component {


    state = {
        source: "",
        playing: false,
        url: "http://"+window.location.host
    }

    componentDidMount() {

    }

    updateSong = () => {
        var url = this.state.url+"/getCommands";
            Request.get(url).then((response) => {
                var commands = response.body;
                console.log(commands);
                if (commands.source.localeCompare(this.state.source) != 0) {
                    this.changeSource(commands.source);
                    this.play();
                }

                if (this.state.playing != commands.playing) {
                    if (commands.playing) {
                        this.play();
                    } else {
                        this.stop();
                    }
                }
            });
    }



    changeSource(source) {
        this.stop();
        this.setState({source: source});
    }

    nextSong() {
        var url = "http://127.0.0.1:3000/nextSong";
        Request.get(url).then((response) => {

        });
    }

    stop() {
        this.setState({source: null, playing: false});
    }

    pause() {
        this.setState({playing: false});
        this.refs.player.audioEl.pause();
    }

    play() {
        this.setState({playing: true});
        this.refs.player.audioEl.play();
    }

    downloadPlayerCommands() {

    }

    render() {
        const {playing, source} = this.state;

        const style = {
            position: "absolute",
            zIndex: "-20",
            top
        };


        return (
            <div>
                <div style={style} id="player">
                    <ReactInterval
                        timeout={1000}
                        enabled={true}
                        callback={() => this.updateSong()}
                    />
                    <ReactAudioPlayer
                        onEnded={() => this.nextSong()}
                        ref="player"
                        src={source}
                        playing={playing}
                    />
                </div>
                <PlayerBox playing={playing} source={this.state.source} color={this.props.color}/>
            </div>

        )
    }

}
export default Player;
