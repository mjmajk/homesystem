import React from "react";
const axios = require('axios');

export default class PlayerBox extends React.Component {
    state = {
        source: "Lojza",
        song: ""
    }

    componentWillMount() {
        console.log(this.props);
        this.setState({song: this.props.source});
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.source);
        var song = nextProps.source;
        if (song != null) {

            var start = song.lastIndexOf("\\") + 1;
            var length = song.length - start - 40;

            var source = song.substr(0, start - 1);
            source = source.substr(source.lastIndexOf("\\") + 1, source.length - source.lastIndexOf("\\"));
            song = song.substr(start, length);

            this.setState({source: source});
            this.setState({song: song});
        }


    }

    render() {
        const divStyle = {
            position: "absolute",
            top: "800px",
            width: "500px",
            color: `${ this.props.color}`,
            textAlign: "left",
            height: "200px",
            left: "710px",
        };

        const sourceStyle = {
            position: "absolute",
            top: "30px",
            left: "130px",
            fontSize: "50px",

        };
        const songStyle = {
            position: "absolute",
            top: "100px",
            left: "130px",
            fontSize: "20px",
        };

        return (

                    <div style={divStyle} className="player-box text-color">
                        <div>
                            <img id="img-play" src="client/images/triangle.png" width="100px" alt=""/>
                        </div>
                        <span style={sourceStyle}>{this.state.source}</span>
                        <span style={songStyle}>{this.state.song}</span>
                    </div>
        );

    }
}
