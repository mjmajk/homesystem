import React from "react";
var axios = require('axios');
var ScrollArea = require('react-scrollbar');


export default class BackgroundImage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            images: {},
            view: false,
            image: "",
            list: 0,
            numOfLists: 0,
            numOfImageOnPage: 7,
            url: "http://"+window.location.host
        }

        this.changeView = this.changeView.bind(this);
        this.componentWillMount = this.componentWillMount.bind(this);
    }

    changeView() {
        this.setState({view: !this.state.view});
        console.log(this.state.view);

    }

    componentWillMount() {
        this.getImage();

        var self = this;
        var url = this.state.url+"/usablebackgrounds";
        axios.get(url)
            .then(function (response) {
                self.setState({images: response.data})
                console.log(self.state.images.length);
                var x = Math.round(self.state.images.length / self.state.numOfImageOnPage);
                console.log(x);
                self.setState({numOfLists: x});
            });
    }

    setImage = (image) => {
        this.setState({image: image});
        this.save(image);
        console.log(image)
    }

    nextImGroup = () => {
        this.setState({list: this.state.list + 1})
        if (this.state.list >= this.state.numOfLists - 1) {
            this.setState({list: 0})
        }

    };

    prevImGroup = () => {
        this.setState({list: this.state.list -1})
        if (this.state.list <= 0) {
            this.setState({list: this.state.numOfLists-1});
        }
    };


    getImage = () => {
        var url = this.state.url+"/getBackgroundImg";
        console.log(url);
        var self = this;
        axios.get(url)
            .then(function (response) {
                    console.log("image: " + response.data)
                    self.setState({image: response.data});
                }
            );

    }


    save = (image) => {
        var url = this.state.url + "/savebackground?background=" + image;
        console.log(url)
        axios.get(url)
            .then(function (response) {
                    console.log(response);
                }
            );
    }


    render() {
        const left = {
            float: "left"
        };
        const divStyle = {
            position: "absolute",
            right: "35px",
            top: "130px",
            zIndex: "4",
            cursor: 'pointer',
        };
        const imagesGroup = {

            width: "1450",
            position: "absolute",
            left: "100px",
            bottom: "20px"


        };
        const bckPicker = {
            position: "absolute",
            width: "1920px",
            height: "160px",
            left: "0px",
            bottom: "0px",
            backgroundColor: "black",
            zIndex: "5",
            color: "white",

        };
        const triangleLeft = {
            position: "absolute",
            zIndex: "40",
            left: "30px",
            bottom: "30px",
            cursor: 'pointer',
        }
        const triangleRigt = {
            position: "absolute",
            left: "1600px",
            bottom: "30px",
            cursor: 'pointer',

        }
        const backgroundImageStyle = {
            position: "absolute",
            width: "1920px",
            top: "0px",
            left: "0px",
            height: "1080px",
            backgroundColor: "white",
            zIndex: "-4",
            backgroundImage: `url(${this.state.image})`

        };
        const bckIcon = {
            cursor: 'pointer',
            zIndex: "10"


        };
        const wrapper = {
            backgroundImage: "black",
        };
        const oval = {
            borderRadius: "50px",
            width: "15px",
            height: "15px",
            backgroundColor: "white",
            float: "left",
            marginLeft: "5px"
        };
        const ovalH = {
            borderRadius: "50px",
            width: "15px",
            height: "15px",
            backgroundColor: "red",
            float: "left",
            marginLeft: "5px"
        };
        const ovals = {
            position: "absolute",
            width: "1920px",
            bottom: "5px",
            left: "100px",
        };
        var indicators = [];
        for (var i = 0; i < this.state.numOfLists; i++) {
            if (i == this.state.list) {
                indicators.push(<div style={ovalH} key={i}></div>);
            } else {
                indicators.push(<div style={oval} key={i}></div>);
            }

        }

        return (
            <div>
                <div style={backgroundImageStyle}>
                </div>
                { this.props.view ?
                    <div>
                        <div onClick={this.changeView} style={divStyle}>
                            <img src="./client/images/background-icon.png" width="60px" alt=""/>
                        </div>

                        { this.state.view ?
                            <div style={bckPicker}>
                                <div >
                                    <img onClick={this.prevImGroup} style={triangleLeft} src="./client/images/left.png"
                                         width="30px" alt=""/>
                                </div>
                                <div style={imagesGroup}>
                                    {this.state.images.map((image, index) => {

                                            return <div style={left}>
                                                <span onClick={() => this.setImage("./client/backgrounds/" + image)}
                                                      style={bckIcon}>
                                            { ((index > this.state.list * this.state.numOfImageOnPage )
                                            && (index < (this.state.list * this.state.numOfImageOnPage) + this.state.numOfImageOnPage) ) ?
                                                <img src={"./client/backgrounds/" + image} width="240px" alt=""/>
                                                : null }

                                                </span>
                                            </div>

                                        }
                                    )}
                                </div>
                                <div >
                                    <img onClick={this.nextImGroup} style={triangleRigt} src="./client/images/right.png"
                                         width="30px" alt=""/>
                                </div >
                                <div style={ovals}>
                                    {indicators}
                                </div>
                            </div>
                            : null }
                    </div>

                    : null }
            </div>
        );
    }
}
