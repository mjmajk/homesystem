import React from "react";
import {fetchAlarmTime} from '../actions/actions.time';
import ReactInterval from 'react-interval';
import Request from "superagent";
const url = window.location.host;
console.log(url);
class AlarmTime extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            alarmTime: "04:30",
            url: "http://"+window.location.host
        }
    };


    downloadAlarmTime() {

        const url = this.state.url+"/getalarmtime";
        console.log(url);
        Request.get(url).then((response) => {
            this.setState({alarmTime: response.body});
        });

    }

    render() {
        const {alarmTime} = this.props;
        const divStyle = {
            color: `${ this.props.color}`,
        };
        return (
            <span>
                <ReactInterval
                    timeout={5000}
                    enabled={true}
                    callback={() => this.downloadAlarmTime()}
                />
                <span style={divStyle} className="alarm-time text-color small-box orbitron">{
                    this.state.alarmTime}
                </span>
            </span>


        );
    }
}

export
default
AlarmTime;
