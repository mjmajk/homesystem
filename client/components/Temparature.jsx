import React from "react";
var axios = require('axios');
import ReactInterval from 'react-interval';

export default class Time extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            temparature: 0
        }
    this.updateTemparature();

    }

    updateTemparature = () => {
        const wetherURL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D'https%3A%2F%2Fpocasi.seznam.cz%2Fplzen%23dnes'%20and%0A%20%20xpath%3D'%2F%2Fspan%5B%40class%3D%22temp%22%5D'&format=json&diagnostics=true&callback=";
        const self = this;
        axios.get(wetherURL)
            .then(function (response) {
                console.log(response.data.query.results.span[0].span[0].content);
                self.setState({temparature: response.data.query.results.span[0].span[0].content});
            });
    }

    render() {

        const divStyle = {
            fontSize: "180px",
            textAlign: "left",
            color: `${ this.props.color}`,
        };
        return (
            <div>
                <ReactInterval
                    timeout={30000}
                    enabled={true}
                    callback={() => this.updateTemparature()}
                />
            <span style={divStyle} className="temp">
          {this.state.temparature}°C</span>
            </div>
        );
    }
}
