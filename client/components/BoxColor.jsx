import React from "react";
import reactCSS from 'reactcss'
import {SketchPicker} from 'react-color'
var axios = require('axios');
export default class BoxColor extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            display: "block",
            displayColorPicker: false,
            color: {
                r: '241',
                g: '112',
                b: '19',
                a: '1',
            },
            url: "http://"+window.location.host
        }


    }

    changeState = () => {
        var color = this.state.color;
        var url = this.state.url + "/getboxcolor";
        console.log(this.state.color);
        axios.get(url)

            .then(res => {
                var color = res.data;
                console.log(color);
                this.setState({color: color});
            });

    }

    componentDidMount() {


        this.changeState();


    }

    handleClick = () => {
        this.setState({displayColorPicker: !this.state.displayColorPicker})
    };

    handleClose = () => {
        this.setState({displayColorPicker: false})
    };


    handleChange = (color) => {
        this.forceUpdate();
        this.setState({color: color.rgb})
        console.log(color.hex);
        var toSend = JSON.stringify(this.state.color);
        var url = this.state.url + "/saveboxcolor?boxcolor=" + toSend;
        console.log("url" + url);

        axios.get(url)
            .then(function (response) {

                }
            );
    };

    render() {
        const styles = reactCSS({
            'default': {
                tempStyle: {
                    position: "absolute",
                    zIndex: "-1",
                    top: "800px",
                    left: "50px",
                    width: "500px",
                    height: "200px",
                    borderRadius: "45px",
                    backgroundColor: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,

                },
                alarmTime: {
                    position: "absolute",
                    zIndex: "-1",
                    top: "800px",
                    right: "50px",
                    width: "500px",
                    height: "200px",
                    borderRadius: "45px",
                    background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,

                },
                player: {
                    position: "absolute",
                    zIndex: "-1",
                    top: "800px",
                    left: "710px",
                    width: "500px",
                    height: "200px",
                    borderRadius: "45px",
                    background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,

                },
                bigBox: {
                    borderRadius: "45px",
                    zIndex: "-1",
                    width: "1800px",
                    height: "700px",
                    top: "50px",
                    left: "50px",
                    position: "absolute",
                    backgroundColor: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,

                },
                color: {
                    width: '36px',
                    height: '14px',
                    borderRadius: '2px',
                    background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
                },
                swatch: {
                    padding: '5px',
                    background: '#fff',
                    borderRadius: '1px',
                    boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                    display: 'block',
                    cursor: 'pointer',
                    position: 'absolute',
                    right: '40px',
                    top: "40px",
                    zIndex: "5"
                },
                popover: {
                    position: 'absolute',
                    zIndex: '20',
                    height: "400px",
                    right: "0px",
                    top: "10px",

                },
                cover: {
                    position: 'fixed',
                    top: '0px',
                    right: '0px',
                    bottom: '0px',
                    left: '0px',
                },
            },
        });
        const colorpicker = {
            position: "absolute",
            right: "0px",
            top: "0px",
            textAlign: "left",
            width: "20px",
            height: "20px",


        };
        const wrapper = {

            width: "150px",
            height: "200px",
            position: "absolute",
            right: "0px",
            top: "0px",
        };
        const popisek = {
            position: "absolute",
            right: "100px",
            width: "80px",
            top: "40px",
            zIndex: "5",
            color: "white"
        };

        return (
            <div>
                <div style={styles.tempStyle}></div>
                <div style={styles.alarmTime}></div>
                <div style={styles.player}></div>
                <div style={styles.bigBox} classname="big-box-text text-color time orbitron"></div>

                { this.props.view ?
                <div style={wrapper}>
                    <span style={popisek}>
                        Barva boxu
                    </span>
                    <div style={colorpicker}>
                        <div>


                                <div style={ styles.swatch } onClick={ this.handleClick }>
                                    <div style={ styles.color }/>
                                </div>

                            { this.state.displayColorPicker ? <div style={ styles.popover }>
                                    <div style={ styles.cover } onClick={ this.handleClose }/>
                                    <SketchPicker color={ this.state.color } onChange={ this.handleChange }/>
                                </div> : null }

                        </div>
                    </div>
                </div>
                    : null }
            </div>

        );
    }
}
